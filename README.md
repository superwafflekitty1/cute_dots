# cute_dots

### Animated Wallpaper

I used [gifdec](https://github.com/lecram/gifdec) alongside some modifications
to dwm in order to set a gif for my wallpaper. I followed [this
tutorial](https://donqustix.github.io/unix/linux/dwm/2020/02/29/set-animated-gif-image-as-dwm-wallpaper.html) to add gif support to dwm.

### Cat cursor

The cat following my cursor is [oneko](https://github.com/glreno/oneko),
specifically the 'tora' cat. 

### Music Player

My favorite music player is [OCP](https://stian.cubic.org/project-ocp.php), it's
a super versatile music player originally built for DOS, but later ported to
linux/windows/bsd/etc. I don't have any customizations applies to OCP yet, but I
plan to make some ui changes in the future.

### Wallpaper

[This](https://en.picmix.com/pic/Garfield-cares-about-nermal-11261785) is the
wallpaper I use, I think it's adorable :3

### Firefox theme

I used to have a lot of custome css and js for my firefox, but it was always
such a hassle to keep up to date, so nowadays I just use [this
theme](https://addons.mozilla.org/en-US/firefox/addon/soothing-pink/)

### Note

This machine has been my daily driver for quite a while at this point, so a lot of
configs are buried away (or just very unorganized). I will continue to add 
things to this repo as time continues, but it's probably not going to be
comprehensive for quite a while. If you have any questions at all, feel free to
dm me on [reddit](https://reddit.com/user/superwafflekitty/) or shoot me an
[email](634587234@protonmail.ch).
